//
//  ProductListViewController.swift
//  fnac
//  Copyright © 2016 Fnac. All rights reserved.

import UIKit
import UIScrollView_InfiniteScroll
import ActionSheetPicker_3_0
import FnacServices


class ProductListViewController: BaseViewController, ProductListView, UICollectionViewDelegate, UICollectionViewDataSource, ProductCollectionViewCellDelegate, FiltersDelegate, RMPZoomTransitionAnimating, RMPZoomTransitionDelegate {

    
    var presenter: ProductListPresenter!
    var listName: String!
    
    var offScreenCells: [String: ProductCollectionViewCell] = [:]
    var cells: [ProductCollectionViewCell] = [ProductCollectionViewCell]()
    let collectionViewInset: CGFloat = 4
    let collectionViewSpacing: CGFloat = 4
    
    var shouldReloadScroll: Bool = true
    
    @IBOutlet var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
        }
    }
    
    @IBOutlet weak var numberOfProductsLabel: UILabel!
    @IBOutlet weak var sortButton: UIButton!
    
    var indexSortSelected: Int = 0
    
    @IBOutlet weak var filterButton: AnimatedRoundedButton!
    
    var headerView: FilterTagsView?
    
    //Fix for strange layout on viewdidload from search (if someone has a better solution..)
    var viewIsLoaded: Bool = false
    
    var hasToResetNavigationFilter: Bool = false
    
    var campaignAccessing: CampaignAccessing?
    @IBOutlet weak var mosaicButton: UIButton!
    
    @IBOutlet weak var toolsView: UIView!
    @IBOutlet weak var toolsViewTopConstraint: NSLayoutConstraint!
    
    var previousScrollViewYOffset: CGFloat = 0.0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        presenter = ProductListPresenter(productListView: self)
    }
    
    func setProductList(productList productList: ProductList?, catalog: Catalog?, keyword: String?, hasToResetNavigationFilter: Bool = false) {
        
        self.hasToResetNavigationFilter = hasToResetNavigationFilter
        
        if let catalog = catalog {
            self.listName = catalog.nodeName
        }
        else {
            self.listName = keyword
        }
        
        presenter.setProductList(productList: productList, catalog: catalog, keyword: keyword)
        presenter.campaignAccessing = campaignAccessing
    }
    
    func setProductList(productList productList: ProductList?,
                                    keyword: String,
                                    filters: [FilterHelperObject]?,
                                    categoryFilterId: String?,
                                    sort: SortProduct?) {
        
        self.hasToResetNavigationFilter = true
        self.listName = keyword
        presenter.setProductList(productList: productList,
                                 catalog: nil,
                                 keyword: keyword,
                                 filters: filters,
                                 categoryFilterId: categoryFilterId,
                                 sort: sort)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fc_localizableStrings()
        
        resetNavigationFilters()
        
        setupCollectionView()
        
        self.mosaicButton.setImage(
            CustomerManager.isInMosaicMode ? UIImage(named: "search_burger") : UIImage(named: "mosaic_view"),
            forState: .Normal)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !viewIsLoaded {
            presenter.viewDidLoad()
            viewIsLoaded = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        opaqueNavBar()
        presenter.trackOmniture()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        presenter.trackPage()
    }
    
    func setupCollectionView() {
        
        collectionView.registerNib(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        collectionView.registerNib(UINib(nibName: "FilterTagsView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "FilterTagsView")
        
        collectionView.setNeedsLayout()
        collectionView.layoutIfNeeded()
        
        collectionView.contentInset = UIEdgeInsets(top: 42, left: 0, bottom: 0, right: 0)
                
        collectionView.addInfiniteScrollWithHandler { [weak self] (scrollView) -> Void in
            self!.presenter.loadProductList()
        }
        
        collectionView.setShouldShowInfiniteScrollHandler { (UICollectionView) -> Bool in
            return self.shouldReloadScroll
        }

    }
    
    func resetNavigationFilters() {
        
        guard hasToResetNavigationFilter else {
            return
        }
                
        var newNavigationViewControllers = self.navigationController!.viewControllers
        newNavigationViewControllers.removeAtIndex(self.navigationController!.viewControllers.count-2)
        newNavigationViewControllers.removeAtIndex(self.navigationController!.viewControllers.count-3)
        newNavigationViewControllers.removeAtIndex(self.navigationController!.viewControllers.count-4)

        self.navigationController!.viewControllers = newNavigationViewControllers
        
        hasToResetNavigationFilter = false
        
    }
    
    
    //MARK:- Presenter
    
    func showSearchLeftTitle() {
        setLeftTitle(self.listName)
        addLeftSearchImage()
    }
    
    func showCatalogLeftTitle() {
        setLeftTitle(self.listName)
    }
    
    func showProducts(beginIndex beginIndex: Int, numberOfProductsAdded: Int) {
        hideLoader()
        
        guard numberOfProductsAdded > 0 else {
            shouldReloadScroll = false
            self.collectionView.finishInfiniteScroll()
            return
        }
        
        var indexPaths = [NSIndexPath]()
        
        for i in beginIndex...beginIndex+numberOfProductsAdded-1 {
            let indexPath = NSIndexPath(forItem: i, inSection: 0)
            indexPaths.append(indexPath)
        }

        collectionView.performBatchUpdates({ () -> Void in
            self.collectionView.insertItemsAtIndexPaths(indexPaths)
            }, completion: { (finished) -> Void in
                self.collectionView.finishInfiniteScroll()
        });
        
        
    }
    
    func deleteProducts(numberOfProductsDeleted: Int) {
        
        guard numberOfProductsDeleted > 0 else {
            return
        }
        
        var indexPaths = [NSIndexPath]()
        
        for i in 0...numberOfProductsDeleted-1 {
            let indexPath = NSIndexPath(forItem: i, inSection: 0)
            indexPaths.append(indexPath)
        }
        
        self.collectionView.performBatchUpdates({ () -> Void in
            self.collectionView.deleteItemsAtIndexPaths(indexPaths)
            }, completion: { (finished) -> Void in
                self.collectionView.finishInfiniteScroll()
        });
        
    }
    
    func showTotalNumberOfProducts(productsCount: Int) {
        
        if productsCount > 500 {
            numberOfProductsLabel.text = NSLocalizedString("plp.results.over_500", comment: "")
        }
        else {
            numberOfProductsLabel.text = String(productsCount)+NSLocalizedString((productsCount < 2 ? "plp.results_singular" : "plp.results_plural"), comment: "")
            numberOfProductsLabel.setColorSubstring(NSLocalizedString((productsCount < 2 ? "plp.results_singular" : "plp.results_plural"), comment: ""), color: UIColor.fcLighterTextColor())
        }
        
    }
    
    func showSortSelected(sortSelected: SortProduct) {
        
        indexSortSelected = SortProduct.allValues.indexOf(sortSelected)!
        sortButton.setTitle(sortSelected.description(), forState: .Normal)
        
    }
    
    func showFiltersTag(filters: [FilterHelperObject]?) {
        
        headerView?.setFilters(filters) { (filter) in
            self.presenter.updateFilters(filters)
        }
                
    }
    
    func hideFiltersTag() {
        collectionView.collectionViewLayout.invalidateLayout()
        headerView?.setNeedsDisplay()
    }
    
    //MARK:- Filters Delegate
    
    func reloadWithFilters(filters: [FilterHelperObject]) {
        numberOfProductsLabel.text = ""
        presenter.updateFilters(filters)
    }
    
    //MARK:- ProductCollectionViewCellDelegate
    func addProductToBasket(product: Product) {
        presenter.addProductToBasket(product)
    }
    
    func showProductAddedToBasket() {
        self.showProductAddedToCartSnackBar()
        self.hideLoader()
    }
    
    
    //MARK:- Collection View
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.productsCount()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductCollectionViewCell", forIndexPath: indexPath) as! ProductCollectionViewCell
        cell.setProduct(presenter.productAtIndex(indexPath.row), delegate: self, isInMosaicMode: CustomerManager.isInMosaicMode)
        cell.delegate = self
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("pdpSegueIdentifier", sender: self)
    }
    
   func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader {
            
            headerView = (collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "FilterTagsView", forIndexPath: indexPath) as! FilterTagsView)
            headerView!.fc_localizableStrings()
            headerView?.backgroundColor = UIColor.clearColor()
            
            return headerView!
        }
        
        return UICollectionReusableView(frame: CGRectZero)
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        var headerHeight: CGFloat = 0.1
        
        if let headerView = self.headerView {
            headerHeight = headerView.collectionView.contentSize.height
        }
        
        return CGSize(width: collectionView.getWidth(), height: headerHeight)
        
    }
    
    //MARK:- Collection Flow Layout Delegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(collectionViewInset, collectionViewInset, collectionViewInset, collectionViewInset)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return collectionViewSpacing
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return collectionViewSpacing
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        var targetWidth = (collectionView.bounds.size.width - (collectionViewSpacing*2) - (collectionViewInset))/2
        var height: CGFloat = 370
        
        if !CustomerManager.isInMosaicMode {
        
            height = 200
            targetWidth = self.collectionView.getWidth()

        }
        
        return CGSize(width: targetWidth, height: height)
        
    }
    
    //MARK:- Transition
    
    func transitionSourceImageView() -> UIImageView! {
        
        if self.collectionView!.indexPathsForSelectedItems()?.count > 0 {
            let cell = self.collectionView.cellForItemAtIndexPath(self.collectionView!.indexPathsForSelectedItems()![0]) as! ProductCollectionViewCell
            
            let imageView = UIImageView(image: cell.productView?.productImageView.image)
            imageView.clipsToBounds = true
            imageView.contentMode = cell.productView!.productImageView!.contentMode
            imageView.userInteractionEnabled = false
            imageView.backgroundColor = UIColor.clearColor()
            
            let rec = cell.productView?.productImageView.convertRect(cell.productView!.productImageView.frame, toView: self.collectionView.superview)
            imageView.frame = rec!
            
            return imageView
        }
        return nil
    }
    
    func transitionSourceBackgroundColor() -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func transitionDestinationImageViewFrame() -> CGRect {
        if self.collectionView!.indexPathsForSelectedItems()?.count > 0 {
            let cell = self.collectionView.cellForItemAtIndexPath(self.collectionView!.indexPathsForSelectedItems()![0]) as! ProductCollectionViewCell
            let rec = cell.productView!.productImageView.convertRect(cell.productView!.productImageView.bounds, toView: self.view)
            return rec
        }
        return CGRectZero
    }
    
    //MARK:- ScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let toolsHeight = self.toolsView.getHeight() + 1
        let scrollOffset = scrollView.contentOffset.y
        
        let oldConstant = self.toolsViewTopConstraint.constant
        if self.previousScrollViewYOffset >= scrollOffset || scrollOffset <= -scrollView.contentInset.top {
            self.toolsViewTopConstraint.constant = 0
        } else {
            self.toolsViewTopConstraint.constant = -toolsHeight
        }
        
        if oldConstant != self.toolsViewTopConstraint.constant {
            UIView.animateWithDuration(0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        self.previousScrollViewYOffset = scrollOffset
        
        filterButton.setScrollViewOffset(scrollOffset)
    }
    
    //MARK:- IBActions
    
    @IBAction func sortPressed(sender: AnyObject) {
        ActionSheetStringPicker.fcShowPickerWithTitle("", rows: SortProduct.allDescriptions,
                                                      initialSelection: 0,
                                                      doneBlock: { (picker, index, value) in
                                                        
                                                        self.presenter.updateSortSelected(SortProduct.allValues[index])
                                                        self.presenter.loadProductList()
                                                        
                }, origin: sender)

    }
    
    @IBAction func mosaicPressed(sender: AnyObject) {
        CustomerManager.isInMosaicMode = !CustomerManager.isInMosaicMode
        
        self.mosaicButton.setImage(
            CustomerManager.isInMosaicMode ? UIImage(named: "search_burger") : UIImage(named: "mosaic_view"),
            forState: .Normal)
        
        TrackingHelper.trackProductListModeChanged()
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
    }
    
    //MARK:- Navigation 
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segue.identifier {
            
        case "plpFilterSegueIdentifier"?:
            TrackingHelper.trackOpenFilter()
            let filtersVC = segue.destinationViewController as? FiltersViewController
            let currentDatas = presenter.currentDatas()
            
            if let currentDatas = currentDatas {
                
                filtersVC?.setProductList(currentDatas.productList,
                                          delegate: self,
                                          keyword: currentDatas.keyword,
                                          catalog: currentDatas.catalog,
                                          sort: currentDatas.sort,
                                          categoryId: currentDatas.categoryFilterId,
                                          initialFilters: currentDatas.initialFilters,
                                          initialPriceFilterRange: currentDatas.initialPriceFilterRange
                )
                
            }
        
        case "pdpSegueIdentifier"?:
            
            let pdpVC = segue.destinationViewController as? ProductDetailsViewController
            
            guard let selectedIndexPaths = self.collectionView?.indexPathsForSelectedItems()
                else {
                    return
            }

            let product = presenter.productAtIndex(selectedIndexPaths[0].row)
            let cellSelected = collectionView.cellForItemAtIndexPath(selectedIndexPaths[0]) as! ProductCollectionViewCell
            pdpVC?.setProduct(product, previewImage: cellSelected.productView?.productImageView.image)
            
        default:
            break
        }
        
        
    }
    
    

}
