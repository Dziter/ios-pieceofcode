//
//  ProductListPresenter.swift
//  fnac
//  Copyright © 2016 Fnac. All rights reserved.

import UIKit
import FnacServices

protocol ProductListView: class, ErrorView, LoaderView {
    
    func showTotalNumberOfProducts(productsCount: Int)
    func showProducts(beginIndex beginIndex: Int, numberOfProductsAdded: Int)
    func showSortSelected(sortSelected: SortProduct)
    func deleteProducts(numberOfProductsDeleted: Int)
    
    func showFiltersTag(filters: [FilterHelperObject]?)
    func hideFiltersTag()
    
    func showSearchLeftTitle()
    func showCatalogLeftTitle()
    
    func showProductAddedToBasket()

}


class ProductListPresenter: NSObject {
    
    
    weak var view: ProductListView?
    var catalog: Catalog?
    var range: String?
    var keyword: String?
    var pageNumber: Int = 1
    
    var productList: ProductList?
    var products: [Product] = [Product] ()
    
    var filters: [FilterHelperObject]?
    
    var sortSelected: SortProduct?
    
    var categoryFilterId: String?
    
    var campaignAccessing: CampaignAccessing?
    
    var initialPriceFilterRange: (min: String, max: String)?
    
    
    init(productListView: ProductListView) {
        super.init()
        view = productListView
    }
    
    func setProductList(productList productList:ProductList?, catalog: Catalog?, keyword: String?, filters: [FilterHelperObject]? = nil, categoryFilterId: String? = nil, sort: SortProduct? = nil) {
        self.productList = productList
        self.catalog = catalog
        self.keyword = keyword
        self.filters = filters
        self.categoryFilterId = categoryFilterId
        self.sortSelected = sort
    }
    
    func viewDidLoad() {
        
        if let productList = self.productList {
            manageProductListResult(productList)
        }
        else {
            loadProductList()
        }
        
        if self.catalog != nil {
            view?.showCatalogLeftTitle()
        }
        else {
            view?.showSearchLeftTitle()
        }
        
    }
    
    func loadProductList() {
        
        if let catalogId = self.catalog?.catalogId {
            loadProductListWithCatalogId(String(catalogId))
        }else if let keyword = self.keyword {
            loadProductListWithKeyword(keyword)
        }
        
    }
    
    func loadProductListWithCatalogId(catalogId: String) {
        
        if self.pageNumber == 1 {
            self.view?.showLoader()
        }
        
        ProductListManager.sharedInstance.getProductList(catalogId: catalogId,
                                                         pageNumber: String(self.pageNumber),
                                                         pageSize: "20",
                                                         sort: sortSelected?.rawValue,
                                                         filters: filters?.formattedFiltersSelected(),
                                                         success: { (productList) in
                                                            
                                                            self.view?.hideLoader()
                                                            
                                                            self.manageProductListResult(productList)
                                                            
            }, failure: { (error) in
                if let fnacError = error where fnacError.code == .NoConnection {
                    self.view?.showNoConnectionError()
                } else {
                    self.view?.showServerError()
                }
                self.view?.hideLoader()
        })

    }
    
    func loadProductListWithKeyword(keyword: String) {
        
        if self.pageNumber == 1 {
            self.view?.showLoader()
        }
        
        ProductListManager.sharedInstance.getProductList(keywords: keyword,
                                                         pageNumber: String(self.pageNumber),
                                                         pageSize: "20",
                                                         sort: sortSelected?.rawValue,
                                                         filters: filters?.formattedFiltersSelected(),
                                                         categoryId: categoryFilterId,
                                                         success: { (productList) in
                                                            
                                                            self.view?.hideLoader()
                                                            
                                                            self.manageProductListResult(productList)
                                                            
            }, failure: { (error) in
                if let fnacError = error where fnacError.code == .NoConnection {
                    self.view?.showNoConnectionError()
                } else {
                    self.view?.showServerError()
                }
                self.view?.hideLoader()
        })
        
    }

    
    private func manageProductListResult(productList: ProductList) {
        
        if initialPriceFilterRange == nil {
            initialPriceFilterRange = productList.initialPriceFilterRange()
        }
        
        self.pageNumber += 1
        self.productList = productList
        
        var numberOfProductsAdded = 0
        var beginIndex = 0
        
        if let newProducts = productList.products {
            numberOfProductsAdded = newProducts.count
            beginIndex = self.products.count == 0 ? 0 : products.count
            self.products.appendContentsOf(newProducts)
        }
        
        self.view?.showProducts(beginIndex: beginIndex, numberOfProductsAdded: numberOfProductsAdded)
        
        self.view?.showTotalNumberOfProducts(productList.pageTotalItems)
        
        trackPage()
        
    }
    
    func currentDatas() ->(productList: ProductList,
        keyword: String?,
        catalog: Catalog?,
        sort: SortProduct?,
        categoryFilterId: String?,
        initialFilters: [FilterHelperObject]?,
        initialPriceFilterRange: (min: String, max: String)?)? {
            
            guard let productList = self.productList else {
                return nil
            }
            
            let datas = (productList : productList,
                         keyword : self.keyword,
                         catalog: self.catalog,
                         sort: self.sortSelected,
                         categoryFilterId: self.categoryFilterId,
                         initialFilters: self.filters,
                         initialPriceFilterRange: self.initialPriceFilterRange
                         )
    
            return datas
    
    }
    
    func updateFilters(filters: [FilterHelperObject]?) {
        removeProducts()
        self.filters = filters
        loadProductList()
        loadFiltersTags()
    }
    
    private func removeProducts() {
        
        let productsCount = self.products.count
        products.removeAll()
        pageNumber = 1
        view?.deleteProducts(productsCount)

    }
    
    func loadFiltersTags() {
        view?.showFiltersTag(filters?.filtersSelected())
    }
    
    func updateSortSelected(sortSelected: SortProduct) {
        self.sortSelected = sortSelected
        self.view?.showSortSelected(sortSelected)
        removeProducts()
    }
    
    func addProductToBasket(product: Product) {
        CartManager.sharedInstance.addProductToCart(product, success: { (cartItemCount) in
            
            self.trackPage()
            
            self.view!.showProductAddedToBasket()
            
            }, failure: { (error) in
                
                if let fnacError = error where fnacError.code == .NoConnection {
                    self.view!.showNoConnectionError()
                } else {
                    self.view!.showServerError()
                }
        })
    }
    
    //MARK:- Products
    
    func productAtIndex(index: Int) -> Product {
        return products[index]
    }
    
    func productsCount() -> Int {
        return products.count
    }
    
    //MARK:- Products 
    
    func trackPage() {
        
        guard let productList = self.productList else {
            
            TCHelper.tagListProduct(self.catalog?.nodeName,
                                    resultCount: 0,
                                    filters: nil,
                                    pageIndex: 0,
                                    pageCount: 0,
                                    products: nil,
                                    keyword: self.keyword)
            
            return
        }
        
        TCHelper.tagListProduct(self.catalog?.nodeName,
                                resultCount: productList.pageTotalItems,
                                filters: self.filters?.formattedSelectedFiltersForTracking(),
                                pageIndex: productList.pageNumber,
                                pageCount: productList.pageCount,
                                products: productList.products,
                                keyword: self.keyword)

    }
    
    func trackOmniture() {
        if let catalog = self.catalog, let catalogName = catalog.nodeName, catalogId = catalog.catalogId {
            if let campaignAccessing = self.campaignAccessing {
                TrackingHelper.trackCatalogScreen(catalogName, id: String(catalogId), campaignAccessing: campaignAccessing)
            } else {
                TrackingHelper.trackCatalogScreen(catalogName, id: String(catalogId))
            }
        }
    }
    
}
